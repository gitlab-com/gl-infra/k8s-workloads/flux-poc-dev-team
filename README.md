# flux-dev-team

This repository is part of the proof-of-concept in this
[repository](https://gitlab.com/gitlab-infra-poc/gitops/flux-repo).

In particular, this repository owns and manages the configurations for the `dev-team` tenant defined
[here](https://gitlab.com/gitlab-infra-poc/gitops/flux-repo/-/tree/main/tenants/base/dev-team).
