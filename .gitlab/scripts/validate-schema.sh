#!/usr/bin/env bash

set -eu
set -o pipefail

# Install kubeconform
curl -fsSL "https://github.com/yannh/kubeconform/releases/latest/download/kubeconform-linux-amd64.tar.gz" | tar xz
chmod +x kubeconform
mv kubeconform /usr/local/bin/kubeconform

# Install kustomize
KUSTOMIZE_VERSION="5.0.1"
curl -fsSL "https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2Fv${KUSTOMIZE_VERSION}/kustomize_v${KUSTOMIZE_VERSION}_linux_amd64.tar.gz" | tar xz
chmod +x kustomize
mv kustomize /usr/local/bin/kustomize


# Download the latest Flux API schemas
echo "[INFO] Downloading Flux OpenAPI schemas"
mkdir -p /tmp/flux-crd-schemas/standalone-strict
curl -fsSL https://github.com/fluxcd/flux2/releases/latest/download/crd-schemas.tar.gz | tar xzf - -C /tmp/flux-crd-schemas/standalone-strict

kubeconform_flags=("-verbose" "-strict" "-ignore-missing-schemas" "-schema-location" "default" "-schema-location" "/tmp/flux-crd-schemas")

# Mirror kustomize-controller build options
kustomize_flags=("--load-restrictor=LoadRestrictionsNone")
kustomize_suffix="kustomization.yaml"

# Validating Kustomize overlay files
find . -type f -name $kustomize_suffix -print0 | while IFS= read -r -d $'\0' file; do
  echo "[INFO] Validating kustomization ${file/%$kustomize_suffix}"
  kustomize build "${file/%$kustomize_suffix}" "${kustomize_flags[@]}" | kubeconform "${kubeconform_flags[@]}"
  if [[ ${PIPESTATUS[0]} != 0 ]]; then
    exit 1
  fi
done
