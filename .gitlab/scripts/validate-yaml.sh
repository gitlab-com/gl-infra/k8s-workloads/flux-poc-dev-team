#!/usr/bin/env bash

set -eu
set -o pipefail

# Install yq
curl -fsSL "https://github.com/mikefarah/yq/releases/latest/download/yq_linux_amd64.tar.gz" | tar xz
chmod +x yq_linux_amd64
mv yq_linux_amd64 /usr/local/bin/yq

# Validating all YAML files
find . -type f -name '*.yaml' -print0 | while IFS= read -r -d $'\0' file; do
  echo "[INFO] Validating $file"
  yq e 'true' "$file" > /dev/null
done
